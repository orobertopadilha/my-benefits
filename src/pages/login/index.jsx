import { Button, Card, CardActions, CardContent, TextField, Typography } from "@mui/material"
import { makeStyles } from "@mui/styles"

const useStyles = makeStyles(theme => ({
    loginWrapper: {
        display: 'flex',
        flex: 1,
        height: '100%',
        direction: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginCard: {
        width: '300px'
    }
}))

const LoginPage = () => {
    const loginStyles = useStyles()


    return (
        <div className={loginStyles.loginWrapper}>
            <Card className={loginStyles.loginCard}>
                <CardContent>
                    <Typography variant="h6" align="center">My Benefits</Typography>
                    <form>
                        <TextField label="Login" variant="outlined" margin="normal" fullWidth />
                        <TextField label="Senha" type="password" margin="normal" fullWidth/>
                    </form>
                </CardContent>
                <CardActions>
                    <Button color="primary" variant="contained" fullWidth>
                        Entrar
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

export default LoginPage