import { Button, Card, CardContent, Container, FormControl, Grid, InputLabel, MenuItem, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import AppToolbar from '../../components/toolbar'

const Home = () => {
    return (
        <>
            <AppToolbar />

            <Container style={{marginTop: 20}}>
                <Typography variant="h5">
                    Saldo de Benefícios
                </Typography> 

                <Grid container spacing={2} style={{marginTop: 10}}>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                            <InputLabel id="benefitCardLabel">Selecione um cartão</InputLabel>
                            <Select
                                labelId="benefitCardLabel"
                                label="Selecione um cartão">
                                <MenuItem value={1}>9999-9999-9999-999</MenuItem>
                                <MenuItem value={2}>9999-9999-9999-999</MenuItem>
                                <MenuItem value={3}>9999-9999-9999-999</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={1}>
                        <Button variant="contained">Pesquisar</Button>
                    </Grid>
                </Grid>

                <Grid container spacing={2} style={{marginTop: 20}}>
                    <Grid item xs={3}>
                        <Card>
                            <CardContent>
                                <Typography variant="h6">Alimentação</Typography>
                                <Typography variant="body1">R$ 99.99</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>

                <Typography variant="body1" style={{marginTop: 36}}>Últimas Transações</Typography>

                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tipo</TableCell>
                                <TableCell>Categoria</TableCell>
                                <TableCell>Valor</TableCell>
                                <TableCell>Data</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>XXXXXXXXXX</TableCell>
                                <TableCell>XXXXXXXXXX</TableCell>
                                <TableCell>R$ 99.99</TableCell>
                                <TableCell>99/99/9999</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>

            </Container>
        </>
    )
}

export default Home;