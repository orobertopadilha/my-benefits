import { Route, Switch, Redirect } from 'react-router-dom';
import Home from '../pages/home'
import Login from '../pages/login'

const AppRouter = () => { 

    return (
        <Switch>
             <Route path="/" exact>
                 <Redirect to="/login" />
             </Route>
             <Route path="/home" component={Home} />
             <Route path="/login" component={Login} />
        </Switch>
    )
}

export default AppRouter