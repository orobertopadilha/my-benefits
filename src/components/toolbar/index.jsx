import { AppBar, Button, Toolbar, Typography } from '@mui/material'
import { useHistory } from 'react-router-dom'

const AppToolbar = () => {
    const history = useHistory()
    
    const exitApp = () => {
        history.push('/login')
    }

    return (
        <AppBar position='static'>
            <Toolbar>
                <Typography variant="h6">
                    My Benefits
                </Typography>
                <span style={{flexGrow: 1}} />
                <Button color="inherit" onClick={exitApp}>Sair</Button>
            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar