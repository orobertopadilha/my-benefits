import api from '../api'

const getTransactions = async (cardId) => {
    const response = await api.get(`/benefits-transactions-api/transaction/${cardId}`)
    return response.data
}

export default getTransactions