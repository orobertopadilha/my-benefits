import api from '../api'

const executeLogin = async (loginData) => {
    try {
        const response = await api.post('/benefits-users-api/login', loginData)
        return response.data
    } catch (exception) {
        throw new Error('Dados inválidos para o login!')
    }
}

export default executeLogin