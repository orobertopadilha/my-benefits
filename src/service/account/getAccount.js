import api from '../api'

const getAccount = async (userId) => {
    const response = await api.get(`/benefits-accounts-api/account/${userId}`)
    return response.data
}

export default getAccount