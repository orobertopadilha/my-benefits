import api from '../api'

const getCards = async (accountId) => {
    const response = await api.get(`/benefits-accounts-api/card/byAccount/${accountId}`)
    return response.data
}

export default getCards