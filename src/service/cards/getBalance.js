import api from '../api'

const getBalance = async (cardId) => {
    const response = await api.get(`/benefits-transactions-api/card/balance/${cardId}`)
    return response.data
}

export default getBalance